# Animation d'une case
# La case a pour coordonnées (i,j) soit (ligne, colonne) dans une grille virtuelle

from tkinter import *
from random import *

def animation():
    global dir_v,dir_h,i,j,case,tab,snake,fruit
    
    if dir_v==1:
        i+=1
    elif dir_v==-1:
        i-=1
    if dir_h==1:
        j+=1
    elif dir_h==-1:
        j-=1
    if i>N-1 or i<0 or j>P-1 or j<0:
        quitter()
        return
    if tab[i][j]==1:
        quitter()
        return
    if tab[i][j]==0:
        cnv.delete(snake[0][1])
        tab[snake[0][0][0]][snake[0][0][1]]=0
        a=snake.pop(0)
    if tab[i][j]==2:
        cnv.delete(fruit)
        i_fruit=randint(0,N-1)
        j_fruit=randint(0,P-1)
        while tab[i_fruit][j_fruit]!=0:
            i_fruit=randint(0,N-1)
            j_fruit=randint(0,P-1)
        tab[i_fruit][j_fruit]=2
        fruit=cnv.create_rectangle(j_fruit*DIM,i_fruit*DIM,j_fruit*DIM+DIM,i_fruit*DIM+DIM,outline='purple',fill='purple')
    case=cnv.create_rectangle(j*DIM,i*DIM,j*DIM+DIM,i*DIM+DIM,outline='red',fill='red')
    snake.append(((i,j),case))
    tab[i][j]=1
    ecran.after(150,animation) # Appel recursif de animation

def haut():
    global dir_v,dir_h
    dir_v=-1
    dir_h=0

def bas():
    global dir_v,dir_h
    dir_v=1
    dir_h=0

def gauche():
    global dir_v,dir_h
    dir_h=-1
    dir_v=0

def droite():
    global dir_v,dir_h
    dir_h=1
    dir_v=0

def quitter():
    ecran.quit()

def on_key_press(event):
    global i,j
    if event.keysym=='Left' and snake[len(snake)-2][0][1]!=j-1:
        gauche()
    elif event.keysym=='Right' and snake[len(snake)-2][0][1]!=j+1:
        droite()
    elif event.keysym=='Up' and snake[len(snake)-2][0][0]!=i-1:
        haut()
    elif event.keysym=='Down' and snake[len(snake)-2][0][0]!=i+1:
        bas()
    elif event.keysym=='Escape':
        quitter()

N=15 # Nombre de lignes
P=15 # Nombres de colonnes
DIM=20 # Dimension d'une case en pixels et longueur d'un saut pour animation

i,j=1,1 # Coordonnées de la tête i : ligne, j : colonne
dir_v,dir_h=0,1 # Directions verticales et horizontales ; au choix -1 ou 0 ou 1.

tab=[[0 for x in range(P)] for _ in range(N)]
tab[i][j]=1

snake=[]

ecran=Tk()
ecran.geometry('-50+300')

# Toile de fond
cnv=Canvas(ecran,bg='green',height=N*DIM,width=P*DIM) ; cnv.pack(side=LEFT)
# Grille (Juste pour voir les cases)
for ligne in range(N): # Horizontales
    cnv.create_line(0,ligne*DIM,P*DIM,ligne*DIM,fill='black',width=1)
for colonne in range(P): # Verticales
    cnv.create_line(colonne*DIM,0,colonne*DIM,N*DIM,fill='black',width=1)

case=cnv.create_rectangle(j*DIM,i*DIM,j*DIM+DIM,i*DIM+DIM,outline='red',fill='red')
snake.append(((i,j),case))

i_fruit=randint(0,N-1)
j_fruit=randint(0,P-1)
while tab[i_fruit][j_fruit]!=0:
    i_fruit=randint(0,N-1)
    j_fruit=randint(0,P-1)
tab[i_fruit][j_fruit]=2
fruit=cnv.create_rectangle(j_fruit*DIM,i_fruit*DIM,j_fruit*DIM+DIM,i_fruit*DIM+DIM,outline='purple',fill='purple')

ecran.bind("<KeyPress>",on_key_press)
animation()
ecran.mainloop()
ecran.destroy()