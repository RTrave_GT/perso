from tkinter import *
from functools import partial
from random import randint
from time import sleep

dim=10
pourcentage_bombes=15
nb_bombes=int((dim**(2))*(pourcentage_bombes/100))+1

def peri8(i,j):
    """
    Renvoie dans une liste les coordonnées des cases au Nord-Ouest, Nord, Nord-Est, Ouest, Est, Sud-Ouest, Sud, Sud-Est des coordonnées (i,j) (colonne, ligne)
    """
    L=[]
    for e in [(-1,-1),(0,-1),(1,-1),(-1,0),(1,0),(-1,1),(0,1),(1,1)]:
        if dim-1>=(i+e[0])>=0 and dim-1>=(j+e[1])>=0:
            L.append((i+e[0],j+e[1]))
    return L

def compte_bombe(i,j):
    """
    Compte les bombes dans la grille autour des coordonnées (i,j)
    """
    s=0
    for e in peri8(i,j):
        if grille[e[0]][e[1]]==1:
            s+=1
    return s

def revele(i,j):
    def recu1(i,j):
        if grille[i][j]==0 and compte_bombe(i,j)==0:
            grille[i][j]=-1
            for e in peri8(i,j):
                recu1(e[0],e[1])
    def recu2(i,j):
        b=boutons[i][j]
        b['text']=compte_bombe(i,j)
        if grille[i][j]==-1:
            grille[i][j]=0
            for e in peri8(i,j):
                recu2(e[0],e[1])
    recu1(i,j)
    recu2(i,j)

def jouer(i,j):
    b=boutons[i][j]
    if grille[i][j]==1:
        b['bg']='red'
        b['text']='💥'
    elif grille[i][j]==0:
        revele(i,j)

def drapeau(event):
    b=event.widget
    b['text']='🚩'

def drapeauRemove(event):
    b=event.widget
    b['text']=''

def quitter():
    ecran.quit()

boutons=[[None for j in range(dim)] for i in range(dim)] # tableau de boutons
grille=[[0 for j in range(dim)] for i in range(dim)]

def init(n):
    # Place aléatoirement n bombes dans grille
    coord_bombes=[]
    while len(coord_bombes)<n:
        (i,j)=(randint(0,dim-1),randint(0,dim-1))
        if (i,j) not in coord_bombes:
            coord_bombes.append((i,j))
    for e in coord_bombes:
        grille[e[0]][e[1]]=1
    # Compte les bombes tout autour de la position (i,j)
    i_start=randint(0,9)
    j_start=randint(0,9)
    while grille[i_start][j_start]!=0:
        i_start=randint(0,9)
        j_start=randint(0,9)
    print(i_start,j_start,grille[i_start][j_start])
#    jouer(i_start,j_start)



init(nb_bombes)
ecran=Tk()
buttonFont=('Tahoma',16)
for i in range(dim):
    for j in range(dim):
        b=Button(ecran,width=2,font=buttonFont)
        b['command']=partial(jouer,i,j)
        b['text']='' # Pour tout voir
        b.bind('<Button-3>',drapeau) # Ajouter un drapeau avec un clic droit
        b.bind('<Button-2>',drapeauRemove)
        b.grid(row=i,column=j)
        boutons[i][j]=b
ecran.mainloop()