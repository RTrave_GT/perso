from random import*

class Pile():
    def __init__(self):
        self.contenu=[]
    def est_vide(self):
        return self.contenu==[]
    def empiler(self,x):
        self.contenu.append(x)
    def dépiler(self):
        if self.est_vide():
            raise IndexError('Pile vide!')
        return self.contenu.pop()
    def __str__(self):
        return str(self.contenu)
    def hauteur(self):
        return len(self.contenu)
    def vider(self):
        self=Pile()

def dcdl():
    op=["+","-","*","//"]
    nombres=[]
    while len(nombres)<6:
        n=randint(1,10)
        if n in nombres:
            continue
        nombres.append(n)
    
    nombres2=nombres.copy()
    nbOperations=randint(1,3)+2
    n1=nombres2.pop(randint(0,len(nombres2)-1))
    for _ in range(nbOperations):
        n2=nombres2.pop(randint(0,len(nombres2)-1))
        opérateur=op[randint(0,len(op)-1)]
        while eval("n1"+opérateur+"n2")==0 or eval("n1"+opérateur+"n2")==n1 or eval("n1"+opérateur+"n2")==n2:
            opérateur=op[randint(0,len(op)-1)]
        n1=eval("n1"+opérateur+"n2")
    
    objectif=n1
    
    progression=Pile()
    nombreActuel=0
    
    while nombreActuel!=objectif:
        réponse=input("Entrez la suite : ")
        if réponse=="retour" and not progression.est_vide():
            e=progression.dépiler()
            if e not in op:
                nombres.append(e)
            # modifier nombreActuel
        elif réponse in op:
            progression.empiler(réponse)
        else:
            print(type(réponse))
            try:
                réponse=int(réponse)
            except ValueError:
                print("Veuillez éviter d'entrer des réponses stupides.")
                continue
            print(type(réponse))
        if réponse in nombres:
            print("rep dans nombres")
            nombres.remove(réponse)
            if progression.hauteur()>=1:
                dernierOp=progression.dépiler()
                dernierNombre=progression.dépiler()
                progression.empiler(dernierOp)
                progression.empiler(dernierNombre)
                nombreActuel=eval(str(dernierNombre)+dernierOp+str(réponse))
            progression.empiler(réponse)
            nombreActuel
        
        else:
            print("Veuillez éviter d'entrer des réponses stupides.")
    print("Vous avez réussi à atteindre le nombre cible ! Bravo !")